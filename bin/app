#!/usr/bin/env php
<?php

use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;
use SymfonyConsole\Console\Application as ConsoleApplication;

ini_set('display_errors', true);
chdir(__DIR__);

$previousDir = '.';

while (!file_exists('config/application.config.php')) {
    $dir = dirname(getcwd());

    if ($previousDir === $dir) {
        throw new RuntimeException(
            'Unable to locate "config/application.config.php"'
        );
    }

    $previousDir = $dir;
    chdir($dir);
}

if (is_readable('init_autoloader.php')) {
    include_once 'init_autoloader.php';
} elseif (file_exists(__DIR__ . '/../vendor/autoload.php')) {
    include_once __DIR__ . '/../vendor/autoload.php';
} elseif (file_exists(__DIR__ . '/../../../autoload.php')) {
    include_once __DIR__ . '/../../../autoload.php';
} else {
    throw new RuntimeException('Error: vendor/autoload.php could not be found. Did you run php composer.phar install?');
}

$appConfig = include 'config/application.config.php';
if (file_exists('config/development.config.php')) {
    $appConfig = ArrayUtils::merge($appConfig, include 'config/development.config.php');
}

$application = Application::init($appConfig);
/** @var Application $app */
$app = Application::init($appConfig);
$serviceManager = $app->getServiceManager();

$console = $serviceManager->get(ConsoleApplication::class);
$console->run();

exit;