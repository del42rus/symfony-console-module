<?php

require_once __DIR__ . '/../Command/InvalidCommand.php';

use Mockery\MockInterface;
use Zend\ServiceManager\ServiceManager;
use SymfonyConsole\Console\Factory\ApplicationFactory;
use SymfonyConsole\Console\Application as ConsoleApplication;

class ApplicationFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var ServiceManager|MockInterface */
    protected $serviceManager;

    /** @var ApplicationFactory */
    protected $factory;

    protected function _before()
    {
        $this->serviceManager = Mockery::mock(ServiceManager::class);
        $this->factory = new ApplicationFactory();
    }

    protected function _after()
    {
        Mockery::close();
    }

    public function testValidConsoleApplicationInstance()
    {
        $this->serviceManager->shouldReceive('get')->with('Config')->andReturn([]);

        $consoleApplication = ($this->factory)($this->serviceManager, ConsoleApplication::class);

        $this->assertInstanceOf(ConsoleApplication::class, $consoleApplication);
    }

    public function testWrongConfig()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Config entry `console` -> `commands` must be an array');

        $config = [
            'console' => [
                'commands' => 'Wrong value'
            ]
        ];

        $this->serviceManager->shouldReceive('get')->with('Config')->andReturn($config);

        ($this->factory)($this->serviceManager, ConsoleApplication::class);
    }

    public function testInvalidCommand()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(sprintf(
            'Command with name %s is not valid. Command must be an implementation of %s',
            InvalidCommand::class,
            'Symfony\Component\Console\Command\Command'
        ));

        $config = [
            'console' => [
                'commands' => [
                    InvalidCommand::class
                ]
            ]
        ];

        $this->serviceManager->shouldReceive('get')->with('Config')->andReturn($config);
        $this->serviceManager->shouldReceive('get')->with(InvalidCommand::class)->andReturn(new InvalidCommand());

        ($this->factory)($this->serviceManager, ConsoleApplication::class);
    }
}