<?php

namespace SymfonyConsole;

use SymfonyConsole\Console\Application;
use SymfonyConsole\Console\Factory\ApplicationFactory;

return [
    'service_manager' => [
        'factories' => [
            Application::class => ApplicationFactory::class,
        ]
    ],
    'console' => [
        'commands' => [
        ]
    ],
];
