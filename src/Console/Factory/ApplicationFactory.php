<?php
namespace SymfonyConsole\Console\Factory;

use SymfonyConsole\Console\Application;
use Interop\Container\ContainerInterface;
use Symfony\Component\Console\Application as SymfonyConsoleApplication;
use Symfony\Component\Console\Command\Command;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApplicationFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $application = new Application(new SymfonyConsoleApplication());

        $config = $container->get('Config');

        $commands = $config['console']['commands'] ?? [];
        if (!is_array($commands)) {
            throw new \RuntimeException('Config entry `console` -> `commands` must be an array');
        }

        foreach ($commands as $commandName) {
            /** @var Command $command */
            $command = $container->get($commandName);
            $this->checkCommand($command, $commandName);
            $application->addCommand($command);
        }

        return $application;
    }

    /**
     * @param mixed $command
     * @param string $commandName
     * @throws \RuntimeException
     */
    private function checkCommand($command, $commandName)
    {
        if (!$command instanceof Command) {
            throw new \RuntimeException(
                sprintf(
                    'Command with name %s is not valid. Command must be an implementation of %s',
                    $commandName,
                    'Symfony\Component\Console\Command\Command'
                )
            );
        }
    }
}