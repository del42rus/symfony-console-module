<?php
namespace SymfonyConsole\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Application as SymfonyConsoleApplication;

class Application
{
    /**
     * @var Command[]
     */
    protected $commands = [];

    /**
     * @var SymfonyConsoleApplication
     */
    protected $app;

    public function __construct(SymfonyConsoleApplication $app)
    {
        $this->app = $app;
    }

    /**
     * @param Command $command
     *
     * @return Application $this
     */
    public function addCommand(Command $command)
    {
        $found = array_search($command, $this->commands);
        if (false !== $found) {
            $this->commands[$found] = $command;
        } else {
            $this->commands[] = $command;
        }

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        foreach ($this->commands as $command) {
            $this->app->add($command);
        }

        $this->app->run();
    }
}